

$(document).ready(function(){

var producto;
/* MODAL PARA DETALLE DE PRODUCTOS */
$(document).on('click', '.enlaceAjaxProducto', function(event) {
    event.preventDefault();
    
    $('#modalProductos').modal('show');

	var $url = $(this).attr('href');
	//$('#modalProductos').modal();
	//producto_cargando();
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: $url,
		data: "{}",
		dataType: "json",
		success: function (response, status) {
			console.log(response);
			/*return false;
			//producto_cargado();
			$('#modalProductos #errores_preload').hide(0);
			$('#modalProductos #producto-tabla_total_precio').hide(0);
			$('#modalProductos #carrito-preload-form').show(0);
			$('#modalProductos #carrito-comprafinalizada').hide(0);
			$('#modalProductos #producto-nombre').html(response.nombre);
			$('#modalProductos #producto-categoria').html(response.nombre_categoria);
			$('#modalProductos #producto-precio').html('$'+currency(response.precio)).removeClass('text-bold').removeClass('text-tachado').addClass(response.class_precio);
			$('#modalProductos #producto-sku').html(response.sku);
			$('#modalProductos #producto-cantidad').html(response.disponible);
			$('#modalProductos #producto-unidad').html(response.UM);
			$('#modalProductos #producto-cantidad-max').html(response.no_mas);

			observaciones_aduanales = $('<textarea />').html(response.observaciones).text();
			$('#modalProductos #producto-observaciones .panel-body').html(observaciones_aduanales);

			$('#modalProductos #producto-subtotal').attr('data-costominpublico', 0);
			cantidad_de_imagenes = 0;
			$.each(response.thumb500, function (indexImagen) {
				$('#producto-imagenes-thumbs #producto-thumbs-'+indexImagen).attr( "src", "" );

				if (response.thumb500[indexImagen] == '0') {
					$('#producto-imagenes-thumbs #producto-thumbs-'+indexImagen).hide(0);
				}
				else {
					cantidad_de_imagenes++;
					$('#producto-imagenes-thumbs #producto-thumbs-'+indexImagen).attr("src",response.thumb500[indexImagen]).attr("title", response.nombre).attr("alt", response.nombre).show(0);
				}
			});
			if (cantidad_de_imagenes < 5) {
				$('#modalProductos .navigation .prev.prev-navigation').hide(0).parent().css({width: '100%'});
				$('#modalProductos .navigation .next.next-navigation').hide(0).parent().css({width: '100%'});
			}
			else {
				$('#modalProductos .navigation .prev.prev-navigation').show(0).parent().css({width: '80%'});
				$('#modalProductos .navigation .next.next-navigation').show(0).parent().css({width: '80%'});
			}


			$('#multizoom-img').attr('src',$('.multizoom-list li img:first').attr('src'));

			$('#input-alias').val(response.alias);
			$('#input-producto').val(response.id);

			$('#favorite-id').attr('href', site_url + 'producto/agregarFavorito/' + response.id);

			if(response.no_menos == 0) { response.no_menos = 1; }
			producto = {
				'cantidad_min'    : response.no_menos,
				'cantidad_max'    : response.no_mas,
				'precio'          : response.precio,
				'peso'            : parseFloat(response.peso_producto),
				'sistema_peso'    : response.sistema_peso,
				'volumen'         : parseFloat(response.altura * response.anchura * response.profundidad),
				'alto'						: response.altura,
				'largo'						: response.profundidad,
				'ancho'						: response.anchura
			};

			if (response.soporta_descuento == "si") {
				$('#modalProductos #producto-descuento-div').show();
				$('#modalProductos #producto-descuento-precio').html(response.descuento_monto);
				$('#modalProductos #producto-descuento-total').html(response.producto_precio_descuento);
				producto.precio = parseFloat(response.producto_precio_descuento_sf);
			}
			else {
				$('#modalProductos #producto-descuento-div').hide();
			}

			// Agrega la cantidad max y minima a comprar
			min = (parseInt(producto.cantidad_min));
			max = (parseInt(producto.cantidad_max - producto.cantidad_min)+1);
			// Options_cantidad = '<option value=""></option>';
			options_cantidad = '';
			for (var i = min; i <= max; i++) {
				options_cantidad += '<option value="'+[i]+'">'+[i]+'</option>';
			}
			$('#modalProductos #producto-cantidad-minmax').html(options_cantidad);

			$('#modalProductos #red_social_shared_facebook').attr('href', "<?php echo RED_SOCIAL_SHARED_FACEBOOK . site_url('producto') ?>" + '/' + response.alias);
			$('#modalProductos #red_social_shared_twitter').attr('href', "<?php echo RED_SOCIAL_SHARED_TWITTER . site_url('producto') ?>" + '/' + response.alias);
			$('#modalProductos #red_social_shared_google').attr('href', "<?php echo RED_SOCIAL_SHARED_GOOGLE . site_url('producto') ?>" + '/' + response.alias);

			$('#modalProductos #producto-empresa-envio').hide(0);
			$('#modalProductos #producto-provincias').hide(0);
			$('#modalProductos #producto-sucursal').hide(0);
			//Agrega los tipos de envio
			options_tipos_envio = '<option value="">Calcula envío</option>';
			if ( response.envio_maritimo == 1 ) { options_tipos_envio += '<option value="envio_maritimo">Recogida directa</option>'; }
			if ( response.envio_aereo == 1 ) { options_tipos_envio += '<option value="envio_aereo">Aereo</option>'; }
			if ( response.envio_express == 1 ) { options_tipos_envio += '<option value="envio_express">Entrega a domicilio</option>'; }
			$('#modalProductos #producto-tipos-envio').html(options_tipos_envio);


			$('#modalProductos #producto-sistema-peso').html(response.sistema_peso);
			$('#modalProductos #producto-sistema-superficie').html(response.sistema_superficie);

			$('#modalProductos #producto-valor-aduanal').html(parseFloat(response.valor_aduanal));

			producto_descripcion = $('<textarea />').html(response.descripcion).text();
			$('#modalProductos #producto-detalles').html(producto_descripcion);

			if (parseInt(response.disponible_menaje) == 0)		{ $('#modalProductos #producto-disponible-menaje').css({'display' : 'none'}); } else { $('#modalProductos #producto-disponible-menaje').show(0) }
			if (parseInt(response.disponible_equipaje) == 0)	{ $('#modalProductos #producto-disponible-equipaje').css({'display' : 'none'}); } else { $('#modalProductos #producto-disponible-equipaje').show(0) }
			if (parseInt(response.disponible_paqueteria) == 0)	{ $('#modalProductos #producto-disponible-paqueteria').css({'display' : 'none'}); } else { $('#modalProductos #producto-disponible-paqueteria').show(0) }
			if (parseInt(response.disponible_diplomatico) == 0)	{ $('#modalProductos #producto-disponible-diplomatico').css({'display' : 'none'}); } else { $('#modalProductos #producto-disponible-diplomatico').show(0) }

			if (response.peso_producto == 0) { $('#modalProductos #producto-peso').parent().hide(0); } else { $('#modalProductos #producto-peso').html(response.peso_producto).parent().show(0); }
			if (producto.volumen == 0) {
				$('#modalProductos #producto-altura').parent().hide(0);
			}
			else {
				$('#modalProductos #producto-altura').html(response.altura).parent().show(0);
				$('#modalProductos #producto-anchura').html(response.anchura);
				$('#modalProductos #producto-profundidad').html(response.profundidad);
			}

			$('#modalProductos input[name=producto_alias]').val(response.alias);*/
			
		},
		error: function (result) {
		}
	});

	//history.pushState('data', '', $url);
});


/* CAPTURADOR DE IMAGEN MULTIZOOM */
$(document).on('click', '.multizoom-list li img', function(event){
    event.preventDefault();
    event.stopPropagation();
    $('.multizoom-list li img').removeClass('border-img');
    $(this).addClass('border-img');
    var img = $(this).attr('src');
    $('#multizoom-img').attr('src',img).css({'width':'100%'});
    //  $('.magnifyarea img').attr('src',img);
});

/** funcionalidad: modal del menu */

	$(document).on('click', '.modalMenuItem', function(event){
		var url = $(this).attr('href');
		event.preventDefault();
		$.get(url, function(response){
			$('#body-modal-menu').html(response);
		})
		$('#modalMenu').modal('show');
	});


});



