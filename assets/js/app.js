"use strict";

$(function() {

  var owls = $(".slider-inner-1").owlCarousel({
          animateOut: 'fadeOut',
          animateIn: 'fadeIn',
          autoplay: true,
          autoplayTimeout: 5000,
          autoplaySpeed:  1000,
          smartSpeed: 500,
          autoplayHoverPause: false,
          startPosition: 0,
          mouseDrag:  true,
          touchDrag: true,
          dots: true,
          autoWidth: false,
          dotClass: "owl2-dot",
          dotsClass: "owl2-dots",
          loop: true,
          navText: ["Next", "Prev"],
          navClass: ["owl2-prev", "owl2-next"],
          responsive: {
            0:{ items: 1,
              nav: true,
            },
            480:{ items: 1,
              nav: true,
            },
            768:{ items: 1,
              nav: true,
            },
            992:{ items: 1,
              nav: true,
            },
            1200:{ items: 1,
              nav: true,
            }
          }
  });


  var owlc = $(".owl-carousel-category").owlCarousel({
    items: 5,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    lazyLoad: true,
    loop: false,
    margin: 20,
    autoWidth: false,
    autoplay: false,
    autoplayTimeout: 5000,
    responsiveClass: true,
    responsive: {
        0:{ items: 1,
          nav: true,
        },
        480:{ items: 2,
          nav: true,
        },
        768:{ items: 4,
          nav: true,
        },
        992:{ items: 4,
          nav: true,
        },
        1200:{ items: 5,
          nav: true,
        }
      }
  });



  var owlp = $(".slider-inner-pacotillas").owlCarousel({
    animateOut: 'fadeInRight',
    animateIn: 'fadeOutLeft',
    autoplay: false,
    autoplayTimeout: 5000,
    autoplaySpeed:  1000,
    smartSpeed: 500,
    autoplayHoverPause: false,
    startPosition: 0,
    mouseDrag:  true,
    touchDrag: true,
    dots: true,
    autoWidth: false,
    dotClass: "owl2-dot",
    dotsClass: "owl2-dots",
    loop: true,
    navText: ["Next", "Prev"],
    navClass: ["owl2-prev", "owl2-next"],
    responsive: {
      0:{ items: 1,
        nav: true,
      },
      480:{ items: 1,
        nav: true,
      },
      768:{ items: 1,
        nav: true,
      },
      992:{ items: 1,
        nav: true,
      },
      1200:{ items: 1,
        nav: true,
      }
    }
});

  var owlt = $(".slider-inner-pacotillas-thumbnail").owlCarousel({
    items: 10,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    lazyLoad: true,
    loop: false,
    margin: 0,
    autoWidth: false,
    autoplay: false,
    autoplayTimeout: 5000,
    responsiveClass: true,
    responsive: {
        0:{ items: 10,
          nav: true,
        },
        480:{ items: 10,
          nav: true,
        },
        768:{ items: 10,
          nav: true,
        },
        992:{ items: 10,
          nav: true,
        },
        1200:{ items: 10,
          nav: true,
        }
      }
  });

  owlt.on('change.owl.carousel', function(event) {
    owlp.trigger('next.owl.carousel');
    $('.slider-inner-pacotillas-thumbnail .item-thumbnail .ds-image-thumb').removeClass('active');
    $('.slider-inner-pacotillas-thumbnail .item-thumbnail .item-'+event.item.index+'').addClass('active');
  });

  owlt.on('click', '.owl-item', function(event) {
    var i = $(this).index();
    $('.slider-inner-pacotillas-thumbnail .item-thumbnail .ds-image-thumb').removeClass('active');
    $('.slider-inner-pacotillas-thumbnail .item-thumbnail .item-'+i+'').addClass('active');
    owlp.trigger('to.owl.carousel',[i , 200, true]);
    
  });

  



  var owl = $(".owl-carousel-d").owlCarousel({
    items: 4,
    lazyLoad: true,
    loop: true,
    margin: 40,
    autoplay: false,
    autoplayTimeout: 5000,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: false
      },
      600: {
        items: 2,
        nav: false
      },
      1000: {
        items: 4,
        nav: false,
        loop: false
      }
    }
  });

  $(".max-next-pro").click(function() {
    owl.trigger("next.owl.carousel");
  });
  $(".max-prev-pro").click(function() {
    owl.trigger("prev.owl.carousel", [300]);
  });

  var owl_m = $(".owl-product-movil").owlCarousel({
    items: 2,
    lazyLoad: true,
    loop: true,
    nav:false,
    margin: 8,
    autoplay: false,
    autoplayTimeout: 5000,
    responsiveClass: true
  });

  $(".op-m .max-next-pro").click(function() {
    owl_m.trigger("next.owl.carousel");
  });
  $(".op-m .max-prev-pro").click(function() {
    owl_m.trigger("prev.owl.carousel", [300]);
  });



  var owll = $(".slider-lista-1").owlCarousel({
    autoplay: false,
    autoplayTimeout: 0,
    autoplaySpeed:  1000,
    smartSpeed: 500,
    autoplayHoverPause: false,
    startPosition: 0,
    mouseDrag:  true,
    touchDrag: true,
    dots: false,
    autoWidth: false,
    dotClass: "owl2-dot",
    dotsClass: "owl2-dots",
    loop: false,
    navText: ["Next", "Prev"],
    navClass: ["owl2-prev", "owl2-next"],
    responsive: {
      0:{ items: 1,
        nav: true,
      },
      480:{ items: 1,
        nav: true,
      },
      768:{ items: 1,
        nav: true,
      },
      992:{ items: 1,
        nav: true,
      },
      1200:{ items: 1,
        nav: true,
      }
    }
});

var owltienda = $(".slider-tienda-asociada").owlCarousel({
  items: 10,
  animateOut: 'fadeOut',
  animateIn: 'fadeIn',
  lazyLoad: true,
  loop: true,
  margin: 15,
  autoWidth: false,
  autoplay: false,
  autoplayTimeout: 5000,
  responsiveClass: true,
  navText: ["Next", "Prev"],
  navClass: ["owl2-prev", "owl2-next"],
  responsive: {
    0:{ items: 2,
      nav: true,
    },
    480:{ items: 2,
      nav: true,
    },
    768:{ items: 6,
      nav: true,
    },
    992:{ items: 6,
      nav: true,
    },
    1200:{ items: 6,
      nav: true,
    }
  }
});

var owltestimonio = $(".slider-testimonio").owlCarousel({
  items: 3,
  lazyLoad: true,
  loop: true,
  margin: 20,
  autoWidth: true,
  autoplay: true,
  autoplayTimeout: 5000,
  responsiveClass: true,
  navClass: ["owl2-prev", "owl2-next"],
  responsive: {
    0:{ items: 1,
      nav: true,
    },
    480:{ items: 1,
      nav: true,
    },
    768:{ items: 2,
      nav: true,
    },
    992:{ items: 3,
      nav: true,
    },
    1200:{ items: 3,
      nav: true,
    }
  }
});





  $('.item-ul-s-menu').click(function(event){
      event.preventDefault();
      if($(this).attr('data-click-state') == 0) {
        $(this).find('.nav-submenu').show();
        $(this).attr('data-click-state', 1);
      }else{
        $(this).find('.nav-submenu').hide();
        $(this).attr('data-click-state', 0);
      }
  });
 
  $(document).on('click', '.menuCategoria', function(event){
    $(".body-background-modal").show();
    $('.list-info-menu').hide();
    $('.list-category').show();
    $('.max-men-sup').removeClass('fadeOutCategory').addClass('fadeCategory');
    
  });

   
  $(document).on('click', '#menuInfo', function(event){
    $(".body-background-modal").show();
    $('.list-category').hide();
    $('.list-info-menu').show();

    $('.max-men-sup').removeClass('fadeOutCategory').addClass('fadeCategory');
    
  });

  // Comprobar si estamos, al menos, 100 px por debajo de la posición top
  // para mostrar o esconder el botón
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $(".scrollTop").fadeIn();
      $('.header-m').addClass('header-m-fixed');
      $('.option-menu').addClass('flex-m').show();
      $('.max-men-sup').addClass('max-men-sup-fixed');
    } else {
      $(".scrollTop").fadeOut();
      $('.header-m').removeClass('header-m-fixed');
      $('.option-menu').removeClass('flex-m').hide();
      $('.max-men-sup').removeClass('max-men-sup-fixed');
    }
  });

  // al hacer click, animar el scroll hacia arriba
  $(".scrollTop, .scrollTop-m").click(function(e) {
    e.preventDefault();
    $("html, body").animate({ scrollTop: 0 }, 800);
  });



  // click login
  $(".login-hover").click(function(event) {
    event.preventDefault();
    $(".login-step-2").hide();
    $(".login-step-3").hide();
    $(".login-step-1").show();
    $(".body-background-modal").show();
    $(".max-login").addClass("animated fadeIn");
    $(".max-login").show();
  });

  $(".login-register").click(function(event) {
    event.preventDefault();
    $(".body-background-modal").show();
    $(".max-login").addClass("animated fadeIn");
    $(".max-login").show();
    $("#max_register").click();
  });

  $(".body-background-modal, .closed-login").click(function(event) {
    event.preventDefault();
    $(".body-background-modal").hide();
    $(".max-login").hide();
    $('.succes-registro').addClass('oculto');
    $('.max-login .alert').hide();
    
    //$('.max-men-sup').removeClass('fadeCategory').addClass('fadeOutCategory');
    history.pushState('data', '', '');
  });


  $(".sig-cart-step").on("click", function(e) {
    e.preventDefault();
    $("#tabCart a")
      .removeClass("active")
      .addClass("disabled");
    $("#nav-2-tab")
      .removeClass("disabled")
      .addClass("active");
    $("#nav-1")
      .removeClass("show")
      .removeClass("active");

    $("#nav-2")
      .addClass("show")
      .addClass("active");
    scrollTopCart();
  });

  $("#cart-step-3").on("click", function(e) {
    e.preventDefault();
    $("#tabCart a")
      .removeClass("active")
      .addClass("disabled");
    $("#nav-3-tab")
      .removeClass("disabled")
      .addClass("active");
    $("#nav-2")
      .removeClass("show")
      .removeClass("active");
    $("#nav-3")
      .addClass("show")
      .addClass("active");
    scrollTopCart();
  });

  $("#cart-step-4").on("click", function(e) {
    e.preventDefault();
    $("#tabCart a")
      .removeClass("active")
      .addClass("disabled");
    $("#nav-4-tab")
      .removeClass("disabled")
      .addClass("active");
    $("#nav-3")
      .removeClass("show")
      .removeClass("active");

    $("#nav-4")
      .addClass("show")
      .addClass("active");
    scrollTopCart();
  });

  $("#cart-step-prev-1").on("click", function(e) {
    e.preventDefault();
    $("#tabCart a")
      .removeClass("active")
      .addClass("disabled");
    $("#nav-1-tab")
      .removeClass("disabled")
      .addClass("active");
    $("#nav-2")
      .removeClass("show")
      .removeClass("active");

    $("#nav-1")
      .addClass("show")
      .addClass("active");
    scrollTopCart();
  });

  $("#cart-step-prev-2").on("click", function(e) {
    e.preventDefault();
    $("#tabCart a")
      .removeClass("active")
      .addClass("disabled");
    $("#nav-2-tab")
      .removeClass("disabled")
      .addClass("active");
    $("#nav-3")
      .removeClass("show")
      .removeClass("active");

    $("#nav-2")
      .addClass("show")
      .addClass("active");
    scrollTopCart();
  });

  $("#cart-step-prev-3").on("click", function(e) {
    e.preventDefault();
    $("#tabCart a")
      .removeClass("active")
      .addClass("disabled");
    $("#nav-3-tab")
      .removeClass("disabled")
      .addClass("active");
    $("#nav-4")
      .removeClass("show")
      .removeClass("active");

    $("#nav-3")
      .addClass("show")
      .addClass("active");
    scrollTopCart();
  });

  // funcionalidad login

  $(document).on("click", "#max_login", function(e) {
    $('.content_login .alert').hide();
    $(".login-step-2").hide();
    $(".login-step-3").hide();
    $(".login-step-1").show();
  });

  $(document).on("click", "#max_register", function(e) {
    $('.content_login .alert').hide();
    $(".login-step-1").hide();
    $(".login-step-3").hide();
    $(".login-step-2").show();
  });

  $(document).on("click", "#max_recover", function(e) {
    $('.content_login .alert').hide();
    $(".login-step-1").hide();
    $(".login-step-2").hide();
    $(".login-step-3").show();
  });

  $(document).on('click', '.closed-modal-product', function(event){
    event.preventDefault();
    $('#modalProductos').modal('closed');
  });

  $(document).on('click', '.close-login-home', function(event){
    event.preventDefault();
    $(".body-background-modal").hide();
    $(".max-login").hide();
    $('.succes-registro').addClass('oculto');
    $('.max-login .alert').hide();
    history.pushState('data', '', ''); 
  });



  var active_menu_bar = false;
  $(".bar_menu").hover(
      function() {
          $('.container-megamenu .vertical-wrapper').show();
          $('.container-megamenu').css({'z-index':'15'});
          $('.vertical ul.megamenu').css({'z-index':'15'});
          $(".body-background-modal").show();
          active_menu_bar = true;
      }
    );

    $(".body-background-modal").hover(
      function() {
        if(active_menu_bar){
          $('.container-megamenu .vertical-wrapper').hide();
          $(".body-background-modal").hide();
          active_menu_bar = false;
        }
      }
    );

  var active_menu = false;
  $(".list-category").hover(
      function() {
        if(!active_menu){
          $('.list-category').css({'z-index':'999999'});
          $('ul.nav-category').finish().slideDown('medium');
          $(".body-background-modal").show();
          active_menu = true;
        }
      },
      function() {
        if(active_menu){
          $('.list-category').css({'z-index':'0'});
          $('ul.nav-category').finish().slideUp('medium');
          $(".body-background-modal").hide();
          active_menu = false;
        }
      }
    );
    
  // click menu perfil
  var active_menu_perfil = false;
  $(".acceso_perfil_menu").hover(
    function() {
      if(!active_menu_perfil){
        $('.max-perfil').css({'z-index':'999999'});
        $('.max-perfil-menu').finish().slideDown('medium');
        $(".body-background-modal").show();
        active_menu_perfil = true;
      }
    },
    function() {
      if(active_menu_perfil){
        $('.max-perfil').css({'z-index':'0'});
        $('.max-perfil-menu').finish().slideUp('medium');
        $(".body-background-modal").hide();
        active_menu_perfil = false;
      }
    }
  );

  $(document).on('click', '.form-control-filtro', function(event){
    event.preventDefault();

    $('.max_filtro_avanzado').toggle();

  });

});

function click_slider(event){

  console.log(event);
}
  

function scrollTopCart() {
  $("html, body").animate({ scrollTop: 190 }, 300);
}
